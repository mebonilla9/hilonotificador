package co.movistar.hiloanalitico.modelo.vo;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 *
 * @author Fabian
 */
public class VistaPagina implements Serializable {

    private String fecha;
    private String pageTitle;
    private String source;
    private String medium;
    private String vistaPagina;
    private String busquedasOrganicas;
    private Long idVista;
    private String origen;

    public VistaPagina() {
    }

    public VistaPagina(String linea, String origen) {
        int i = 0;
        String[] atributos = linea.split(Pattern.quote("\\"));
        this.fecha = atributos[i++];
        this.pageTitle = atributos[i++];
        this.source = atributos[i++];
        this.medium = atributos[i++];
        this.vistaPagina = atributos[i++];
        this.busquedasOrganicas = atributos[i++];
        this.origen = origen;
    }
    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * @param pageTitle the pageTitle to set
     */
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * @param medium the medium to set
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * @return the vistaPagina
     */
    public String getVistaPagina() {
        return vistaPagina;
    }

    /**
     * @param vistaPagina the vistaPagina to set
     */
    public void setVistaPagina(String vistaPagina) {
        this.vistaPagina = vistaPagina;
    }

    /**
     * @return the busquedasOrganicas
     */
    public String getBusquedasOrganicas() {
        return busquedasOrganicas;
    }

    /**
     * @param busquedasOrganicas the busquedasOrganicas to set
     */
    public void setBusquedasOrganicas(String busquedasOrganicas) {
        this.busquedasOrganicas = busquedasOrganicas;
    }

    /**
     * @return the idVista
     */
    public Long getIdVista() {
        return idVista;
    }

    /**
     * @param idVista the idVista to set
     */
    public void setIdVista(Long idVista) {
        this.idVista = idVista;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }
    
    

}
