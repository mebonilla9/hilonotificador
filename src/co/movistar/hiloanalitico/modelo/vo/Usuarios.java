package co.movistar.hiloanalitico.modelo.vo;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 *
 * @author Fabian
 */
public class Usuarios implements Serializable {

    private String fecha;
    private String source;
    private String medium;
    private String usuarios;
    private String nuevosUsuarios;
    private String sesionesPorUsuario;
    private Long idUsuario;
    private String origen;

    public Usuarios() {
    }
    
    public Usuarios(String linea, String origen){
        int i = 0;
        String[] atributos = linea.split(Pattern.quote("\\"));
        this.fecha = atributos[i++];
        this.source = atributos[i++];
        this.medium = atributos[i++];
        this.usuarios = atributos[i++];
        this.nuevosUsuarios = atributos[i++];
        this.sesionesPorUsuario = atributos[i++];
        this.origen = origen;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * @param medium the medium to set
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * @return the usuarios
     */
    public String getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(String usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the nuevosUsuarios
     */
    public String getNuevosUsuarios() {
        return nuevosUsuarios;
    }

    /**
     * @param nuevosUsuarios the nuevosUsuarios to set
     */
    public void setNuevosUsuarios(String nuevosUsuarios) {
        this.nuevosUsuarios = nuevosUsuarios;
    }

    /**
     * @return the sesionesPorUsuario
     */
    public String getSesionesPorUsuario() {
        return sesionesPorUsuario;
    }

    /**
     * @param sesionesPorUsuario the sesionesPorUsuario to set
     */
    public void setSesionesPorUsuario(String sesionesPorUsuario) {
        this.sesionesPorUsuario = sesionesPorUsuario;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }
    
    
}
