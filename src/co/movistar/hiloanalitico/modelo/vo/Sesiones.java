package co.movistar.hiloanalitico.modelo.vo;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 *
 * @author Fabian
 */
public class Sesiones implements Serializable {

    private String fecha;
    private String browser;
    private String source;
    private String medium;
    private String sesiones;
    private Long idSesion;
    private String origen;

    public Sesiones() {
    }

    public Sesiones(String linea, String origen) {
        int i = 0;
        String[] atributos = linea.split(Pattern.quote("\\"));
        this.fecha = atributos[i++];
        this.browser = atributos[i++];
        this.source = atributos[i++];
        this.medium = atributos[i++];
        this.sesiones = atributos[i++];
        this.origen = origen;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the browser
     */
    public String getBrowser() {
        return browser;
    }

    /**
     * @param browser the browser to set
     */
    public void setBrowser(String browser) {
        this.browser = browser;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * @param medium the medium to set
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * @return the sesiones
     */
    public String getSesiones() {
        return sesiones;
    }

    /**
     * @param sesiones the sesiones to set
     */
    public void setSesiones(String sesiones) {
        this.sesiones = sesiones;
    }

    /**
     * @return the idSesion
     */
    public Long getIdSesion() {
        return idSesion;
    }

    /**
     * @param idSesion the idSesion to set
     */
    public void setIdSesion(Long idSesion) {
        this.idSesion = idSesion;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

}
