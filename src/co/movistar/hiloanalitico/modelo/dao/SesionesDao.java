package co.movistar.hiloanalitico.modelo.dao;

import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import co.movistar.hiloanalitico.modelo.dao.crud.SesionesCrud;
import static co.movistar.hiloanalitico.modelo.dao.crud.SesionesCrud.getSesiones;
import co.movistar.hiloanalitico.modelo.vo.Sesiones;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class SesionesDao extends SesionesCrud {

    public SesionesDao(Connection cnn) {
        super(cnn);
    }

        public Sesiones consultarPorFecha(LocalDate fecha, String origen) throws SQLException {
        PreparedStatement sentencia = null;
        Sesiones obj = null;
        try {
            String sql = "select top 1 * from Vista_pagina where fecha like ? and Origen = ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, fecha.toString());
            sentencia.setString(2, origen);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getSesiones(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }
}
