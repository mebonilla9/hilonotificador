package co.movistar.hiloanalitico.modelo.dao;

import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import java.sql.Connection;
import co.movistar.hiloanalitico.modelo.dao.crud.UsuariosCrud;
import static co.movistar.hiloanalitico.modelo.dao.crud.UsuariosCrud.getUsuarios;
import co.movistar.hiloanalitico.modelo.vo.Usuarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class UsuariosDao extends UsuariosCrud {

    public UsuariosDao(Connection cnn) {
        super(cnn);
    }

    public Usuarios consultarPorFecha(LocalDate fecha, String origen) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios obj = null;
        try {

            String sql = "select top 1 * from Usuarios where fecha like ? and Origen = ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, fecha.toString());
            sentencia.setString(2, origen);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getUsuarios(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }
}
