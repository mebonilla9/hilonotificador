package co.movistar.hiloanalitico.modelo.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import co.movistar.hiloanalitico.modelo.vo.VistaPagina;

public class VistaPaginaCrud implements IGenericoDao<VistaPagina> {

    protected final int ID = 1;
    protected Connection cnn;

    public VistaPaginaCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(VistaPagina VistaPagina) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Vista_pagina(fecha,pageTitle,source,medium,Vista_pagina,Busquedas_Organicas,Origen) values (?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, VistaPagina.getFecha());
            sentencia.setObject(i++, VistaPagina.getPageTitle());
            sentencia.setObject(i++, VistaPagina.getSource());
            sentencia.setObject(i++, VistaPagina.getMedium());
            sentencia.setObject(i++, VistaPagina.getVistaPagina());
            sentencia.setObject(i++, VistaPagina.getBusquedasOrganicas());
            sentencia.setObject(i++, VistaPagina.getOrigen());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                VistaPagina.setIdVista(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(VistaPagina VistaPagina) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Vista_pagina set fecha=?,pageTitle=?,source=?,medium=?,Vista_pagina=?,Busquedas_Organicas=?,Origen=? where Id_vista=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, VistaPagina.getFecha());
            sentencia.setObject(i++, VistaPagina.getPageTitle());
            sentencia.setObject(i++, VistaPagina.getSource());
            sentencia.setObject(i++, VistaPagina.getMedium());
            sentencia.setObject(i++, VistaPagina.getVistaPagina());
            sentencia.setObject(i++, VistaPagina.getBusquedasOrganicas());
            sentencia.setObject(i++, VistaPagina.getOrigen());
            sentencia.setObject(i++, VistaPagina.getIdVista());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<VistaPagina> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<VistaPagina> lista = new ArrayList<>();
        try {

            String sql = "select * from Vista_pagina";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVistaPagina(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public VistaPagina consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        VistaPagina obj = null;
        try {

            String sql = "select * from Vista_pagina where Id_vista=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVistaPagina(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static VistaPagina getVistaPagina(ResultSet rs) throws SQLException {
        VistaPagina VistaPagina = new VistaPagina();
        VistaPagina.setFecha(rs.getString("fecha"));
        VistaPagina.setPageTitle(rs.getString("pageTitle"));
        VistaPagina.setSource(rs.getString("source"));
        VistaPagina.setMedium(rs.getString("medium"));
        VistaPagina.setVistaPagina(rs.getString("Vista_pagina"));
        VistaPagina.setBusquedasOrganicas(rs.getString("Busquedas_Organicas"));
        VistaPagina.setIdVista(rs.getLong("Id_vista"));
        VistaPagina.setOrigen(rs.getString("Origen"));

        return VistaPagina;
    }

    public static VistaPagina getVistaPagina(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        VistaPagina VistaPagina = new VistaPagina();
        Integer columna = columnas.get("Vista_pagina_fecha");
        if (columna != null) {
            VistaPagina.setFecha(rs.getString(columna));
        }
        columna = columnas.get("Vista_pagina_pageTitle");
        if (columna != null) {
            VistaPagina.setPageTitle(rs.getString(columna));
        }
        columna = columnas.get("Vista_pagina_source");
        if (columna != null) {
            VistaPagina.setSource(rs.getString(columna));
        }
        columna = columnas.get("Vista_pagina_medium");
        if (columna != null) {
            VistaPagina.setMedium(rs.getString(columna));
        }
        columna = columnas.get("Vista_pagina_Vista_pagina");
        if (columna != null) {
            VistaPagina.setVistaPagina(rs.getString(columna));
        }
        columna = columnas.get("Vista_pagina_Busquedas_Organicas");
        if (columna != null) {
            VistaPagina.setBusquedasOrganicas(rs.getString(columna));
        }
        columna = columnas.get("Vista_pagina_Id_vista");
        if (columna != null) {
            VistaPagina.setIdVista(rs.getLong(columna));
        }
        columna = columnas.get("Vista_pagina_Origen");
        if (columna != null) {
            VistaPagina.setOrigen(rs.getString(columna));
        }
        return VistaPagina;
    }

}
