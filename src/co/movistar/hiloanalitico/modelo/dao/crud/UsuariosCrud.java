package co.movistar.hiloanalitico.modelo.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import co.movistar.hiloanalitico.modelo.vo.Usuarios;

public class UsuariosCrud implements IGenericoDao<Usuarios> {

    protected final int ID = 1;
    protected Connection cnn;

    public UsuariosCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Usuarios Usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Usuarios(fecha,source,medium,Usuarios,Nuevos_Usuarios,Sesiones_por_Usuario,Origen) values (?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, Usuarios.getFecha());
            sentencia.setObject(i++, Usuarios.getSource());
            sentencia.setObject(i++, Usuarios.getMedium());
            sentencia.setObject(i++, Usuarios.getUsuarios());
            sentencia.setObject(i++, Usuarios.getNuevosUsuarios());
            sentencia.setObject(i++, Usuarios.getSesionesPorUsuario());
            sentencia.setObject(i++, Usuarios.getOrigen());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                Usuarios.setIdUsuario(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Usuarios Usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Usuarios set fecha=?,source=?,medium=?,Usuarios=?,Nuevos_Usuarios=?,Sesiones_por_Usuario=?,Origen=? where Id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, Usuarios.getFecha());
            sentencia.setObject(i++, Usuarios.getSource());
            sentencia.setObject(i++, Usuarios.getMedium());
            sentencia.setObject(i++, Usuarios.getUsuarios());
            sentencia.setObject(i++, Usuarios.getNuevosUsuarios());
            sentencia.setObject(i++, Usuarios.getSesionesPorUsuario());
            sentencia.setObject(i++, Usuarios.getOrigen());
            sentencia.setObject(i++, Usuarios.getIdUsuario());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<Usuarios> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Usuarios> lista = new ArrayList<>();
        try {

            String sql = "select * from Usuarios";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getUsuarios(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Usuarios consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios obj = null;
        try {

            String sql = "select * from Usuarios where Id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getUsuarios(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static Usuarios getUsuarios(ResultSet rs) throws SQLException {
        Usuarios Usuarios = new Usuarios();
        Usuarios.setFecha(rs.getString("fecha"));
        Usuarios.setSource(rs.getString("source"));
        Usuarios.setMedium(rs.getString("medium"));
        Usuarios.setUsuarios(rs.getString("Usuarios"));
        Usuarios.setNuevosUsuarios(rs.getString("Nuevos_Usuarios"));
        Usuarios.setSesionesPorUsuario(rs.getString("Sesiones_por_Usuario"));
        Usuarios.setIdUsuario(rs.getLong("Id_usuario"));
        Usuarios.setOrigen(rs.getString("Origen"));

        return Usuarios;
    }

    public static Usuarios getUsuarios(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        Usuarios Usuarios = new Usuarios();
        Integer columna = columnas.get("Usuarios_fecha");
        if (columna != null) {
            Usuarios.setFecha(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_source");
        if (columna != null) {
            Usuarios.setSource(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_medium");
        if (columna != null) {
            Usuarios.setMedium(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Usuarios");
        if (columna != null) {
            Usuarios.setUsuarios(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Nuevos_Usuarios");
        if (columna != null) {
            Usuarios.setNuevosUsuarios(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Sesiones_por_Usuario");
        if (columna != null) {
            Usuarios.setSesionesPorUsuario(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Id_usuario");
        if (columna != null) {
            Usuarios.setIdUsuario(rs.getLong(columna));
        }
        columna = columnas.get("Usuarios_Origen");
        if (columna != null) {
            Usuarios.setOrigen(rs.getString(columna));
        }
        return Usuarios;
    }

}
