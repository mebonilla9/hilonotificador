package co.movistar.hiloanalitico.modelo.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import co.movistar.hiloanalitico.modelo.vo.Sesiones;

public class SesionesCrud implements IGenericoDao<Sesiones> {

    protected final int ID = 1;
    protected Connection cnn;

    public SesionesCrud(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Sesiones Sesiones) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Sesiones(fecha,browser,source,medium,Sesiones,Origen) values (?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, Sesiones.getFecha());
            sentencia.setObject(i++, Sesiones.getBrowser());
            sentencia.setObject(i++, Sesiones.getSource());
            sentencia.setObject(i++, Sesiones.getMedium());
            sentencia.setObject(i++, Sesiones.getSesiones());
            sentencia.setObject(i++, Sesiones.getOrigen());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                Sesiones.setIdSesion(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Sesiones Sesiones) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Sesiones set fecha=?,browser=?,source=?,medium=?,Sesiones=?,Origen=? where Id_sesion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, Sesiones.getFecha());
            sentencia.setObject(i++, Sesiones.getBrowser());
            sentencia.setObject(i++, Sesiones.getSource());
            sentencia.setObject(i++, Sesiones.getMedium());
            sentencia.setObject(i++, Sesiones.getSesiones());
            sentencia.setObject(i++, Sesiones.getOrigen());
            sentencia.setObject(i++, Sesiones.getIdSesion());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<Sesiones> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Sesiones> lista = new ArrayList<>();
        try {

            String sql = "select * from Sesiones";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getSesiones(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Sesiones consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Sesiones obj = null;
        try {

            String sql = "select * from Sesiones where Id_sesion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getSesiones(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static Sesiones getSesiones(ResultSet rs) throws SQLException {
        Sesiones Sesiones = new Sesiones();
        Sesiones.setFecha(rs.getString("fecha"));
        Sesiones.setBrowser(rs.getString("browser"));
        Sesiones.setSource(rs.getString("source"));
        Sesiones.setMedium(rs.getString("medium"));
        Sesiones.setSesiones(rs.getString("Sesiones"));
        Sesiones.setIdSesion(rs.getLong("Id_sesion"));
        Sesiones.setOrigen(rs.getString("Origen"));

        return Sesiones;
    }

    public static Sesiones getSesiones(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        Sesiones Sesiones = new Sesiones();
        Integer columna = columnas.get("Sesiones_fecha");
        if (columna != null) {
            Sesiones.setFecha(rs.getString(columna));
        }
        columna = columnas.get("Sesiones_browser");
        if (columna != null) {
            Sesiones.setBrowser(rs.getString(columna));
        }
        columna = columnas.get("Sesiones_source");
        if (columna != null) {
            Sesiones.setSource(rs.getString(columna));
        }
        columna = columnas.get("Sesiones_medium");
        if (columna != null) {
            Sesiones.setMedium(rs.getString(columna));
        }
        columna = columnas.get("Sesiones_Sesiones");
        if (columna != null) {
            Sesiones.setSesiones(rs.getString(columna));
        }
        columna = columnas.get("Sesiones_Id_sesion");
        if (columna != null) {
            Sesiones.setIdSesion(rs.getLong(columna));
        }
        columna = columnas.get("Sesiones_Origen");
        if (columna != null) {
            Sesiones.setOrigen(rs.getString(columna));
        }
        return Sesiones;
    }

}
