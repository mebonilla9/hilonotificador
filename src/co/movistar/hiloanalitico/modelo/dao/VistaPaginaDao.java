package co.movistar.hiloanalitico.modelo.dao;

import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import java.sql.Connection;
import co.movistar.hiloanalitico.modelo.dao.crud.VistaPaginaCrud;
import static co.movistar.hiloanalitico.modelo.dao.crud.VistaPaginaCrud.getVistaPagina;
import co.movistar.hiloanalitico.modelo.vo.VistaPagina;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class VistaPaginaDao extends VistaPaginaCrud {

    public VistaPaginaDao(Connection cnn) {
        super(cnn);
    }
    
    public VistaPagina consultarPorFecha(LocalDate fecha, String origen) throws SQLException{
        PreparedStatement sentencia = null;
        VistaPagina obj = null;
        try {

            String sql = "select * from Vista_pagina where Id_vista=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, fecha.toString());
            sentencia.setString(2, origen);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVistaPagina(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }
}
