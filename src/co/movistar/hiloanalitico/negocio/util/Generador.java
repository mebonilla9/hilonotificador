/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.util;

import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import co.movistar.hiloanalitico.modelo.dto.ReporteUsuariosDto;
import co.movistar.hiloanalitico.modelo.vo.Sesiones;
import co.movistar.hiloanalitico.modelo.vo.Usuarios;
import co.movistar.hiloanalitico.modelo.vo.VistaPagina;
import co.movistar.hiloanalitico.negocio.constantes.ELlaves;
import co.movistar.hiloanalitico.negocio.delegado.SesionesDelegado;
import co.movistar.hiloanalitico.negocio.delegado.UsuariosDelegado;
import co.movistar.hiloanalitico.negocio.delegado.VistaPaginaDelegado;
import co.movistar.hiloanalitico.negocio.excepciones.HiloAnaliticoException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.swing.JLabel;
import javax.swing.JTextPane;

/**
 *
 * @author Lord_Nightmare
 */
public class Generador {

    public void iniciar(JLabel ultimaFecha, JTextPane txtLog) throws InterruptedException, HiloAnaliticoException, SQLException {
        Connection cnn = ConexionBD.conectar();
        try {
            LocalDate fecha = LocalDate.now().minusDays(1);
            txtLog.setText(txtLog.getText() + "\n" + "Generacion de carga para el dia: " + fecha.toString());
            Usuarios usuarioAyer = new UsuariosDelegado(cnn, null).consultarPorFecha(fecha, ELlaves.PUBLICA.getOrigen());
            if (usuarioAyer == null) {
                manejarPorMes(cnn, ELlaves.PUBLICA, fecha, "ga:pageviews,ga:organicSearches", "Vista pagina, Busquedas Organicas", "ga:pageTitle,ga:source,ga:medium", "Vista_pagina");
                manejarPorMes(cnn, ELlaves.PUBLICA, fecha, "ga:sessions", "Sesiones", "ga:browser,ga:source,ga:medium", "Sesiones");
                manejarPorMes(cnn, ELlaves.PUBLICA, fecha, "ga:users,ga:newUsers,ga:sessionsPerUser", "Usuarios,Nuevos Usuarios,Sesiones por Usuario", "ga:source,ga:medium", "Usuarios");

                txtLog.setText(txtLog.getText() + "\n" + "Valores Sitio Publico Generado");
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            }

            usuarioAyer = new UsuariosDelegado(cnn, null).consultarPorFecha(fecha, ELlaves.PRIVADA.getOrigen());
            if (usuarioAyer == null) {
                manejarPorMes(cnn, ELlaves.PRIVADA, fecha, "ga:pageviews,ga:organicSearches", "Vista pagina, Busquedas Organicas", "ga:pageTitle,ga:source,ga:medium", "Vista_pagina");
                manejarPorMes(cnn, ELlaves.PRIVADA, fecha, "ga:sessions", "Sesiones", "ga:browser,ga:source,ga:medium", "Sesiones");
                manejarPorMes(cnn, ELlaves.PRIVADA, fecha, "ga:users,ga:newUsers,ga:sessionsPerUser", "Usuarios,Nuevos Usuarios,Sesiones por Usuario", "ga:source,ga:medium", "Usuarios");

                txtLog.setText(txtLog.getText() + "\n" + "Valores Sitio Mi Fijo Generado");
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            }

            usuarioAyer = new UsuariosDelegado(cnn, null).consultarPorFecha(fecha, ELlaves.MIMOVISTAR.getOrigen());
            if (usuarioAyer == null) {
                manejarPorMes(cnn, ELlaves.MIMOVISTAR, fecha, "ga:pageviews,ga:organicSearches", "Vista pagina, Busquedas Organicas", "ga:pageTitle,ga:source,ga:medium", "Vista_pagina");
                manejarPorMes(cnn, ELlaves.MIMOVISTAR, fecha, "ga:sessions", "Sesiones", "ga:browser,ga:source,ga:medium", "Sesiones");
                manejarPorMes(cnn, ELlaves.MIMOVISTAR, fecha, "ga:users,ga:newUsers,ga:sessionsPerUser", "Usuarios,Nuevos Usuarios,Sesiones por Usuario", "ga:source,ga:medium", "Usuarios");

                txtLog.setText(txtLog.getText() + "\n" + "Valores Sitio Mi Movil Generado");
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            }

            usuarioAyer = new UsuariosDelegado(cnn, null).consultarPorFecha(fecha, ELlaves.APP_NUEVA.getOrigen());
            if (usuarioAyer == null) {
                manejarPorMes(cnn, ELlaves.APP_NUEVA, fecha, "ga:pageviews,ga:organicSearches", "Vista pagina, Busquedas Organicas", "ga:pageTitle,ga:source,ga:medium", "Vista_pagina.csv");
                manejarPorMes(cnn, ELlaves.APP_NUEVA, fecha, "ga:sessions", "Sesiones", "ga:browser,ga:source,ga:medium", "Sesiones.csv");
                manejarPorMes(cnn, ELlaves.APP_NUEVA, fecha, "ga:users,ga:newUsers,ga:sessionsPerUser", "Usuarios,Nuevos Usuarios,Sesiones por Usuario", "ga:source,ga:medium", "Usuarios.csv");

                txtLog.setText(txtLog.getText() + "\n" + "Valores Sitio App Movil Generado");
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            }
            cnn.commit();
            txtLog.setText(txtLog.getText() + "\n" + "Registros Confirmados");
            ultimaFecha.setText("");
            ultimaFecha.setText("Ultima fecha generada: " + fecha.toString());
            txtLog.setText(txtLog.getText() + "\n" + "Ultima fecha generada: " + fecha.toString());

        } catch (IOException | GeneralSecurityException e) {
            cnn.rollback();
            txtLog.setText(txtLog.getText()+"\n"+"Error: "+e.toString());
            e.printStackTrace(System.err);
        } finally {
            ConexionBD.desconectar(cnn);
        }
    }

    private void manejarPorMes(Connection cnn, ELlaves eLlaves, LocalDate fecha, String metrica, String alias, String dimension, String nombre) throws GeneralSecurityException, IOException, HiloAnaliticoException {
        StringBuilder reporteMes = new StringBuilder();
        reporteMes.append(generar(eLlaves, fecha.toString(), fecha.toString(), metrica, alias, dimension));
        importar(cnn, eLlaves, nombre, reporteMes);
    }

    public StringBuilder generar(ELlaves llave, String fechaInicio, String fechaFin, String metrica, String alias, String dimension) throws GeneralSecurityException, IOException {
        GeneradorReporteDinamico r = new ReporteUsuariosDto(llave);
        StringBuilder reporte = r.convertirReporteString(r.generarReporteUsuarios(fechaInicio, fechaFin, metrica, alias, dimension, null));
        return reporte;
    }

    public void importar(Connection cnn, ELlaves llave, String nombre, StringBuilder reporte) throws HiloAnaliticoException {
        switch (nombre) {
            case "Vista_pagina":
                guardarVistas(cnn, llave, reporte);
                break;
            case "Sesiones":
                guardarSesiones(cnn, llave, reporte);
                break;
            case "Usuarios":
                guardarUsuarios(cnn, llave, reporte);
                break;
        }
    }

    private void guardarVistas(Connection cnn, ELlaves llave, StringBuilder reporte) throws HiloAnaliticoException {
        List<VistaPagina> listaPaginas = new ArrayList<>();
        String[] registros = reporte.toString().split(Pattern.quote("\n"));
        for (int i = 0; i < registros.length; i++) {
            listaPaginas.add(new VistaPagina(registros[i], llave.getOrigen()));
        }
        VistaPaginaDelegado vistaPaginaDelegado = new VistaPaginaDelegado(cnn, null);
        for (int i = 0; i < listaPaginas.size(); i++) {
            vistaPaginaDelegado.insertar(listaPaginas.get(i));
        }
    }

    private void guardarSesiones(Connection cnn, ELlaves llave, StringBuilder reporte) throws HiloAnaliticoException {
        List<Sesiones> listaSesiones = new ArrayList<>();
        String[] registros = reporte.toString().split(Pattern.quote("\n"));
        for (int i = 0; i < registros.length; i++) {
            listaSesiones.add(new Sesiones(registros[i], llave.getOrigen()));
        }
        SesionesDelegado sesionesDelegado = new SesionesDelegado(cnn, null);
        for (int i = 0; i < listaSesiones.size(); i++) {
            sesionesDelegado.insertar(listaSesiones.get(i));
        }
    }

    private void guardarUsuarios(Connection cnn, ELlaves llave, StringBuilder reporte) throws HiloAnaliticoException {
        List<Usuarios> listaUsuarios = new ArrayList<>();
        String[] registros = reporte.toString().split(Pattern.quote("\n"));
        for (int i = 0; i < registros.length; i++) {
            listaUsuarios.add(new Usuarios(registros[i], llave.getOrigen()));
        }
        UsuariosDelegado usuariosDelegado = new UsuariosDelegado(cnn, null);
        for (int i = 0; i < listaUsuarios.size(); i++) {
            usuariosDelegado.insertar(listaUsuarios.get(i));
        }
    }

}
