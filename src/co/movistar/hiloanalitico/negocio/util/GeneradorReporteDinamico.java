/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.util;

import co.movistar.hiloanalitico.negocio.constantes.EFiltroTrafico;
import co.movistar.hiloanalitico.negocio.constantes.ELlaves;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.AnalyticsReportingScopes;
import com.google.api.services.analyticsreporting.v4.model.DateRange;
import com.google.api.services.analyticsreporting.v4.model.DateRangeValues;
import com.google.api.services.analyticsreporting.v4.model.Dimension;
import com.google.api.services.analyticsreporting.v4.model.DimensionFilter;
import com.google.api.services.analyticsreporting.v4.model.DimensionFilterClause;
import com.google.api.services.analyticsreporting.v4.model.GetReportsRequest;
import com.google.api.services.analyticsreporting.v4.model.GetReportsResponse;
import com.google.api.services.analyticsreporting.v4.model.Metric;
import com.google.api.services.analyticsreporting.v4.model.Report;
import com.google.api.services.analyticsreporting.v4.model.ReportRequest;
import com.google.api.services.analyticsreporting.v4.model.ReportRow;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Lord_Nightmare
 */
public class GeneradorReporteDinamico {

    private final String APPLICATION_NAME = "Hello Analytics Reporting";
    private final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private final String KEY_FILE_LOCATION;
    protected final String VIEW_ID;

    protected String fechaInicio;
    protected String fechaFin;
    protected String alias;

    public GeneradorReporteDinamico(ELlaves llave) {
        KEY_FILE_LOCATION = llave.getRutaLlave();
        VIEW_ID = llave.getVista();
    }

    protected AnalyticsReporting inicializarReporteAnaliticas() throws GeneralSecurityException, IOException {
        HttpTransport transporte = GoogleNetHttpTransport.newTrustedTransport();
        /*NetHttpTransport.Builder builder = new NetHttpTransport.Builder();
        builder.trustCertificates(GoogleUtils.getCertificateTrustStore());
        builder.setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(System.getProperty("http.proxyHost"), Integer.parseInt(System.getProperty("http.proxyPort")))));
        HttpTransport transporte = builder.build();*/

        GoogleCredential credencial = GoogleCredential
                .fromStream(new FileInputStream(KEY_FILE_LOCATION))
                .createScoped(AnalyticsReportingScopes.all());

        // Construct the Analytics Reporting service object.
        return new AnalyticsReporting.Builder(transporte, JSON_FACTORY, credencial)
                .setApplicationName(APPLICATION_NAME).build();
    }

    public GetReportsResponse generarReporteUsuarios(String fechaInicio, String fechaFin, String metrica, String alias, String dimension, EFiltroTrafico filtro) throws GeneralSecurityException, IOException {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.alias = alias;

        AnalyticsReporting reporte = inicializarReporteAnaliticas();

        DateRange rangoFechas = new DateRange();
        rangoFechas.setStartDate(fechaInicio);
        rangoFechas.setEndDate(fechaFin);

        List<Metric> listaMetrics = new ArrayList<>();
        String[] metricas = metrica.split(Pattern.quote(","));
        String[] aliases = alias.split(Pattern.quote(","));
        for (int i = 0; i < metricas.length; i++) {
            listaMetrics.add(new Metric().setExpression(metricas[i]).setAlias(aliases[i]));
        }

        List<Dimension> listaDimensiones = new ArrayList<>();
        if (!dimension.isEmpty()) {
            String[] dimensiones = dimension.split(Pattern.quote(","));
            for (int i = 0; i < dimensiones.length; i++) {
                listaDimensiones.add(new Dimension().setName(dimensiones[i]));
            }
        }

        List<DimensionFilterClause> dfcl = new ArrayList<>();
        if (filtro != null) {
            dfcl.add(generarFiltrosMetricas(filtro));
        }

        ReportRequest peticion = new ReportRequest()
                .setViewId(this.VIEW_ID)
                .setDateRanges(Arrays.asList(rangoFechas))
                .setMetrics(listaMetrics)
                .setDimensions(listaDimensiones)
                .setDimensionFilterClauses(dfcl);

        List<ReportRequest> peticiones = new ArrayList<>();
        peticiones.add(peticion);
        
        GetReportsRequest obtenerReporte = new GetReportsRequest()
                .setReportRequests(peticiones);

        GetReportsResponse respuesta = reporte.reports().batchGet(obtenerReporte).execute();

        return respuesta;
    }

    public StringBuilder convertirReporteString(GetReportsResponse respuesta) throws IOException {
        StringBuilder reporteTexto = new StringBuilder();
        for (Report reporte : respuesta.getReports()) {

            List<ReportRow> filas = reporte.getData().getRows();

            if (filas == null) {
                reporteTexto.append("No data found for ").append(VIEW_ID);
                return reporteTexto;
            }

            for (ReportRow row : filas) {

                List<DateRangeValues> metrics = row.getMetrics();
                List<String> dimensiones = row.getDimensions();

                reporteTexto.append(fechaInicio.equalsIgnoreCase("today") ? new SimpleDateFormat("yyyy-MM-dd").format(new Date()) : fechaInicio);
                reporteTexto.append("\\");
                for (int i = 0; i < dimensiones.size(); i++) {
                    reporteTexto.append(dimensiones.get(i));
                    reporteTexto.append("\\");
                }

                for (int j = 0; j < metrics.size(); j++) {
                    DateRangeValues values = metrics.get(j);
                    for (int k = 0; k < values.getValues().size(); k++) {
                        reporteTexto.append(values.getValues().get(k));
                        if (k == values.getValues().size() - 1) {
                            reporteTexto.append("\n");
                        } else {
                            reporteTexto.append("\\");
                        }
                    }
                }
            }
        }
        return reporteTexto;
    }

    private DimensionFilterClause generarFiltrosMetricas(EFiltroTrafico filtro) {
        String[] filtros = filtro.getFiltros().split(Pattern.quote("|"));
        List<DimensionFilter> lista = new ArrayList<>();
        DimensionFilterClause mfcl = new DimensionFilterClause().setOperator("OR");
        for (int i = 0; i < filtros.length; i++) {
            DimensionFilter mf = new DimensionFilter().setDimensionName("ga:pagePath").setOperator("ENDS_WITH").setExpressions(Arrays.asList(filtros));
            lista.add(mf);
        }
        mfcl.setFilters(lista);
        return mfcl;
    }

}
