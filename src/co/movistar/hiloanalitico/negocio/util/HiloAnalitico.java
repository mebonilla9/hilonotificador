/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.util;

import co.movistar.hiloanalitico.negocio.excepciones.HiloAnaliticoException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import javax.swing.JLabel;
import javax.swing.JTextPane;

/**
 *
 * @author Lord_Nightmare
 */
public class HiloAnalitico extends Thread {

    private final JLabel ultimaFecha;
    private final JTextPane txtLog;
    private final Generador generador;

    public HiloAnalitico(JLabel ultimaFecha, JTextPane txtLog) {
        this.ultimaFecha = ultimaFecha;
        this.txtLog = txtLog;
        generador = new Generador();
    }

    @Override
    public void run() {
        try {
            while (true) {
                generador.iniciar(ultimaFecha, txtLog);
                txtLog.setText(txtLog.getText() + "\n" + "Tarea Completada");
                Thread.sleep(TimeUnit.HOURS.toMillis(6));
            }
        } catch (InterruptedException | SQLException | HiloAnaliticoException e) {
            StringWriter w = new StringWriter();
            e.printStackTrace(new PrintWriter(w));
            e.printStackTrace(System.err);
            if (e instanceof HiloAnaliticoException) {
                txtLog.setText(txtLog.getText() + "\n" + "Error: " + ((HiloAnaliticoException) e).getMensaje());
            } else {
                //txtLog.setText(txtLog.getText() + "\n" + "Error: " + e.getMessage());
                txtLog.setText(w.toString());
            }
        }
    }

}
