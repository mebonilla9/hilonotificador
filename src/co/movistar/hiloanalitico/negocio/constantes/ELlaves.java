/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.constantes;

import java.io.File;

/**
 *
 * @author Lord_Nightmare
 */
public enum ELlaves {

    PUBLICA("17122469", System.getProperty("user.home") + File.separator + "llavePublica.json", "publica"),
    PRIVADA("82832005", System.getProperty("user.home") + File.separator + "llavePrivada.json", "mifijo"),
    MIMOVISTAR("82830224", System.getProperty("user.home") + File.separator + "llavePrivada.json", "mimovil"),
    APP_NUEVA("140024454", System.getProperty("user.home") + File.separator + "llavePrivada.json", "appnueva"),
    CHATBOT("166482056",System.getProperty("user.home") + File.separator + "llaveChatbot.json", "chatbot");

    private String vista;
    private String rutaLlave;
    private String origen;

    private ELlaves(String vista, String rutaLlave, String origen) {
        this.vista = vista;
        this.rutaLlave = rutaLlave;
        this.origen = origen;
    }

    /**
     * @return the rutaLlave
     */
    public String getRutaLlave() {
        return rutaLlave;
    }

    /**
     * @param rutaLlave the rutaLlave to set
     */
    public void setRutaLlave(String rutaLlave) {
        this.rutaLlave = rutaLlave;
    }

    /**
     * @return the vista
     */
    public String getVista() {
        return vista;
    }

    /**
     * @param vista the vista to set
     */
    public void setVista(String vista) {
        this.vista = vista;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the carpeta to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

}
