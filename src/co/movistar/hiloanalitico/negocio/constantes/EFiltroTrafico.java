/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.constantes;

/**
 *
 * @author Lord_Nightmare
 */
public enum EFiltroTrafico {
    
    FIJO("/movistar-play/html/Interna-suscribete.html|/oferta-hbo/|/descubre/internet-hogar|/cambio-de-plan-fija|/servicios-valor-agregado-internet/|/servicios-valor-agregado-telefonia/|/servicios-valor-agregado-television/|/descubre/television/planes-tv/|/descubre/fijo/local-y-nacional/|/full-hogar-bogota/|/ofertas-full-hogar/|/planes-hogar/|/descubre/servicios-adicionales-hogar|/descubre/television/programacion|/descubre/fijo/larga-distancia-internacional|/fibra/|/cotizador-productos/productos/|/ficha-internet-10mb|/ficha-internet-20mb|/ficha-internet-40mb|/ficha-internet-telefonia-fija-5mb|/ficha-internet-telefonia-fija-10mb|/ficha-internet-telefonia-fija-20mb|/ficha-internet-telefonia-fija-40mb|/ficha-trio-5mb|/ficha-trio-10mb|/ficha-trio-20mb|/ficha-trio-40-megas|/fibra-optica/trios/10megas|/fibra-optica/trios/20megas|/fibra-optica/trios/50megas|/fibra-optica/trios/100megas|/fibra-optica/duos/10megas|/fibra-optica/duos/20megas|/fibra-optica/duos/50megas|/fibra-optica/duos/100megas|/television/paquetes-hd/hd-total|/television/paquetes-hd/hd-plus|/television/plan-zafiro|/television/plan-diamante|/ofertas-fijas-online|/fibra-optica|/internet-telefonia-television|/internet-television|/internet-telefonia|/internet-banda-ancha|/television|/ofertas/hogar/multioferta/|/ofertas/hogar/"),
    MOVIL("/ficha-pospago-1.2gb|/ficha-pospago-2.5gb|/ficha-pospago-3.5gb|/ficha-pospago-plan-exclusivo-portabilidad|/ficha-pospago-4.2gb|/ficha-pospago-7gb|/ficha-pospago-11gb|/ficha-plan-1.2gb/ficha-plan-exclusivo-online|/ficha-plan-portabilidad|/ficha-plan-4.2gb|/ficha-plan-7gb|/ficha-plan-11gb|/cambia-tu-celular|/pospago|/portabilidad|/outlet-movistar?oferta=Camisetas|/outlet-movistar|/black-week-huawei|/tienda|/planes-celulares|/portabilidad-pospago|/descubre/planes-ilimitados/|/plan-ilimitado-exclusivo-online/|/outlet-movistar|/oferta_2x1/landing-2x1.html|/web/rediseno-colombia/descuento-celulares/|/web/rediseno-colombia/oferta-chaquetas-movistar-team|/ofertas/movil");
    
    private final String filtros;

    private EFiltroTrafico(String filtros) {
        this.filtros = filtros;
    }

    /**
     * @return the filtros
     */
    public String getFiltros() {
        return filtros;
    }
    
    
}
