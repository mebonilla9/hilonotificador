/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.delegado;

import co.movistar.hiloanalitico.negocio.excepciones.HiloAnaliticoException;
import co.movistar.hiloanalitico.modelo.dao.UsuariosDao;
import co.movistar.hiloanalitico.modelo.vo.Usuarios;
import co.movistar.hiloanalitico.modelo.dto.AuditoriaDto;
import co.movistar.hiloanalitico.negocio.constantes.EMensajes;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 *
 * @author Fabian
 */
public class UsuariosDelegado extends GenericoDelegado<Usuarios> {

    private final UsuariosDao usuariosDAO;

    public UsuariosDelegado(Connection cnn, AuditoriaDto auditoria) throws HiloAnaliticoException {
        super(cnn, auditoria);
        usuariosDAO = new UsuariosDao(cnn);
        genericoDAO = usuariosDAO;
    }

    public Usuarios consultarPorFecha(LocalDate fecha, String origen) throws HiloAnaliticoException {
        try {
            return usuariosDAO.consultarPorFecha(fecha, origen);
        } catch (SQLException e) {
            throw new HiloAnaliticoException(EMensajes.ERROR_CONSULTAR);
        }
    }
}
