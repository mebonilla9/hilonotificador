/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.delegado;

import co.movistar.hiloanalitico.negocio.excepciones.HiloAnaliticoException;
import co.movistar.hiloanalitico.modelo.dao.SesionesDao;
import co.movistar.hiloanalitico.modelo.vo.Sesiones;
import co.movistar.hiloanalitico.modelo.dto.AuditoriaDto;
import co.movistar.hiloanalitico.negocio.constantes.EMensajes;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 *
 * @author Fabian
 */
public class SesionesDelegado extends GenericoDelegado<Sesiones> {

    private final SesionesDao sesionesDAO;

    public SesionesDelegado(Connection cnn,AuditoriaDto auditoria) throws HiloAnaliticoException {
        super(cnn,auditoria);
        sesionesDAO = new SesionesDao(cnn);
        genericoDAO = sesionesDAO;
    }
   
    public Sesiones consultarPorFecha(LocalDate fecha, String origen) throws HiloAnaliticoException{
        try {
            return sesionesDAO.consultarPorFecha(fecha, origen);
        } catch (SQLException e) {
            throw new HiloAnaliticoException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
