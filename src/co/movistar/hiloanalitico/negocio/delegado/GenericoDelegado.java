/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.delegado;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import co.movistar.hiloanalitico.negocio.constantes.EMensajes;
import co.movistar.hiloanalitico.negocio.excepciones.HiloAnaliticoException;
import co.movistar.hiloanalitico.modelo.conexion.ConexionBD;
import co.movistar.hiloanalitico.modelo.dto.AuditoriaDto;
import co.movistar.hiloanalitico.modelo.dao.crud.IGenericoDao;

/**
 *
 * @author Fabian
 */
public abstract class GenericoDelegado<T> {

    protected Connection cnn;
    protected IGenericoDao genericoDAO;
    protected boolean confirmar = true;

    public GenericoDelegado(Connection cnn, AuditoriaDto auditoriaDTO) throws HiloAnaliticoException {
        this.cnn = cnn;
    }

    public void insertar(T entidad) throws HiloAnaliticoException {
        try {
            genericoDAO.insertar(entidad);
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new HiloAnaliticoException(EMensajes.ERROR_INSERTAR);
        }
    }

    public void editar(T entidad) throws HiloAnaliticoException {
        try {
            genericoDAO.editar(entidad);

        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new HiloAnaliticoException(EMensajes.ERROR_MODIFICAR);
        }
    }

    public List<T> consultar() throws HiloAnaliticoException {
        try {
            return genericoDAO.consultar();
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new HiloAnaliticoException(EMensajes.ERROR_CONSULTAR);
        }
    }

    public T consultar(long id) throws HiloAnaliticoException {
        try {
            return (T) genericoDAO.consultar(id);
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new HiloAnaliticoException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
