/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.hiloanalitico.negocio.delegado;

import co.movistar.hiloanalitico.negocio.excepciones.HiloAnaliticoException;
import co.movistar.hiloanalitico.modelo.dao.VistaPaginaDao;
import co.movistar.hiloanalitico.modelo.vo.VistaPagina;
import co.movistar.hiloanalitico.modelo.dto.AuditoriaDto;
import co.movistar.hiloanalitico.negocio.constantes.EMensajes;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 *
 * @author Fabian
 */
public class VistaPaginaDelegado extends GenericoDelegado<VistaPagina> {

    private final VistaPaginaDao vistaPaginaDAO;

    public VistaPaginaDelegado(Connection cnn,AuditoriaDto auditoria) throws HiloAnaliticoException {
        super(cnn,auditoria);
        vistaPaginaDAO = new VistaPaginaDao(cnn);
        genericoDAO = vistaPaginaDAO;
    }
   
    public VistaPagina consultarPorFecha(LocalDate fecha, String origen) throws HiloAnaliticoException{
        try {
            return vistaPaginaDAO.consultarPorFecha(fecha, origen);
        } catch (SQLException e) {
            throw new HiloAnaliticoException(EMensajes.ERROR_CONSULTAR);
        }
    }
}
